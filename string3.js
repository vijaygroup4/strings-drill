//function to get month from date
function getMonthFromDate(string) {
  let newString = string.split("/");
  let month = Number(newString[1]);

  //checking for valid month
  if (month >= 1 && month <= 12) {
    return month;
  } else {
    return null;
  }
}

//exporting the above function
module.exports = getMonthFromDate;
