//function to split into numeric
function splitTheString(string) {
  //checking if string contains other than numbers and .
  if (/[^0-9.]/.test(string) == true) {
    return [];
  }

  let newStringArray = string.split(".");
  let flag = 0;
  if (newStringArray.length != 4) {
    return [];
  }

  let newArray = newStringArray.map((element) => {
    if (element < 0 || element > 255) {
      flag = 1;
    }
    return Number(element);
  });
  if (flag == 1) {
    return [];
  } else {
    return newArray;
  }
}

//exporting the above function
module.exports = splitTheString;
