//function to get title case
function getTitleCase(object) {
  let firstName = object.first_name || "";
  let middleName = object.middle_name || "";
  let lastName = object.last_name || "";
  firstName =
    firstName.charAt(0).toUpperCase() + firstName.slice(1).toLowerCase();
  middleName =
    middleName.charAt(0).toUpperCase() + middleName.slice(1).toLowerCase();
  lastName = lastName.charAt(0).toUpperCase() + lastName.slice(1).toLowerCase();

  //joining names and removing falthy values
  let newNameString = [firstName, middleName, lastName]
    .filter(Boolean)
    .join(" ");
  return newNameString;
}

//exporting the above function
module.exports = getTitleCase;
