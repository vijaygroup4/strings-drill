//function to convert string to number
function stringToNumber(string) {
  let newString = string.replace(/[^0-9.-]/g, "");
  let number = Number(newString);

  //checking if it is valid or not
  if (isNaN(number) == false) {
    return number;
  } else {
    return 0;
  }
}

//exporting the above function
module.exports = stringToNumber;
