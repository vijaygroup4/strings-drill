//function to convert array to string
function convertArrayToString(array) {
  if (array == []) {
    return [];
  }

  //adding . at end
  array[array.length - 1] = array[array.length - 1] + ".";

  return array.join(" ");
}

//exporting the above function
module.exports = convertArrayToString;
