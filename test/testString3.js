//importing function from string3.js
let getMonthFromDate = require("../string3");

let month = getMonthFromDate("20/1/2021");

if (month) {
  console.log(month);
} else {
  console.log("Invalid month");
}
