// importing function from string4.js
let getTitleCase = require("../string4");

let object = { first_name: "JoHN", middle_name: "doe", last_name: "SMith" };

let titleCaseName = getTitleCase(object);

console.log(titleCaseName);
